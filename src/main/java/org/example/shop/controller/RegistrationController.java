package org.example.shop.controller;

import org.example.shop.database.service.RoleService;
import org.example.shop.database.service.UserService;
import org.example.shop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/authorization")
    public ModelAndView authorization() {

//        bookService.saveBook(new Book("Лев Толстой", "Анна Каренина", Style.RUSSIAN_CLASSIC, 1234));
//        bookService.saveBook(new Book("Стивен Кинг", "Сияние", Style.HORROR, 1242));
//        bookService.saveBook(new Book("Джон Толкин", "Властелин колец", Style.FANTASY, 444));
//        bookService.saveBook(new Book("Кто-то", "50 оттенков серого", Style.ROMANTIC, 300));

        return new ModelAndView("authorization");
    }

    @RequestMapping(value = "/registration")
    public ModelAndView registration() {

        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject(new User());

        return modelAndView;
    }

//    @PostMapping("/registration")
//    public String addUser(@ModelAttribute("user") @Valid User userForm, Model model) {
//
//        List<User> users = userService.findAllUsers();
//        List<User> notAnonUsers = new ArrayList<>();
//        for (User user : users) {
//            if (user.getUsername() != null) notAnonUsers.add(user);
//        }
//
//        boolean exists = false;
//
//        for (User user : notAnonUsers) {
//            if (user.getUsername().equals(userForm.getUsername())) {
//                exists = true;
//                break;
//            }
//        }
//
//        if (exists){
//            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
//            return "registration";
//        } else {
//            userForm.setRole(roleService.findRoleById(3));
//            userService.saveUser(userForm);
//        }
//
//
//        return "redirect:/";
//    }
}
