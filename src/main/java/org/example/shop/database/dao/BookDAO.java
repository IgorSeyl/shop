package org.example.shop.database.dao;

import org.example.shop.model.Book;
import org.example.shop.model.Style;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Book> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Book").list();
    }

    public Book findById(int id) {

        return sessionFactory.getCurrentSession().get(Book.class, id);
    }

    public List<Book> findByAuthor(String author){

        String query = "from Book where author='" + author + "'";

        return sessionFactory.getCurrentSession().createQuery(query, Book.class).list();
    }

    public List<Book> findByBookName(String bookName){

        String query = "from Book where bookName='" + bookName + "'";

        return sessionFactory.getCurrentSession().createQuery(query, Book.class).list();
    }

    public List<Book> findByStyle(Style style){

        String query = "from Book where style='" + style + "'";

        return sessionFactory.getCurrentSession().createQuery(query, Book.class).list();
    }

    public List<Book> findByPrice(float price){

        String query = "from Book where price='" + price + "'";

        return sessionFactory.getCurrentSession().createQuery(query, Book.class).list();
    }

    public void save(Book book) {

        sessionFactory.getCurrentSession().save(book);
    }

    public void update(Book book) {

        sessionFactory.getCurrentSession().update(book);
    }

    public void delete(Book book) {

        sessionFactory.getCurrentSession().delete(book);
    }
}
