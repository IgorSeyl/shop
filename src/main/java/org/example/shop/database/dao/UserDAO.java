package org.example.shop.database.dao;

import org.example.shop.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<User> findAll() {

        return sessionFactory.getCurrentSession().createQuery("from User ").list();
    }

    public User findById(int id) {

        return sessionFactory.getCurrentSession().get(User.class, id);
    }

    public User findByUsername(String username){
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User ").list();
        User result = null;

        for (User user : users) {
            if (user.getUsername().equals(username)) result = user;
        }

        return result;
    };

    public void save(User user) {

        sessionFactory.getCurrentSession().save(user);
    }

    public void update(User user) {

        sessionFactory.getCurrentSession().update(user);
    }

    public void delete(User user) {

        sessionFactory.getCurrentSession().delete(user);
    }
}
