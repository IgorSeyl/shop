package org.example.shop.database.service;

import org.example.shop.database.dao.BookDAO;
import org.example.shop.model.Book;
import org.example.shop.model.Style;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService { // по идее он должен реализовывать интерфейс

    @Autowired
    private BookDAO bookDao;

    public List<Book> findAllBooks() {
        return bookDao.findAll();
    }

    public Book findBookById(int id) {
        return bookDao.findById(id);
    }

    public List<Book> findBookByAuthor(String author) {
        return bookDao.findByAuthor(author);
    }

    public List<Book> findBookByName(String bookName) {
        return bookDao.findByBookName(bookName);
    }

    public List<Book> findByStyle(Style style) {
        return bookDao.findByStyle(style);
    }

    public List<Book> findBookByPrice(float price) { return bookDao.findByPrice(price);}

    public void saveBook(Book book) {
        bookDao.save(book);
    }

    public void updateBook(Book book) {
        bookDao.update(book);
    }

    public void deleteBook(Book book) {
        bookDao.delete(book);
    }

}
