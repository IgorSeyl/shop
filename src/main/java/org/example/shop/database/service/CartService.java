package org.example.shop.database.service;

import org.example.shop.model.Cart;
import org.example.shop.database.dao.CartDAO;
import org.example.shop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {

    @Autowired
    private CartDAO cartDAO;

    public List<Cart> findAll() {
        return cartDAO.findAll();
    }

    public Cart findByUser(User user) {return cartDAO.findByUser(user);}

    public void save(Cart cart) {
        cartDAO.save(cart);
    }

    public void update(Cart cart) {
        cartDAO.update(cart);
    }

    public void delete(Cart cart) {
        cartDAO.delete(cart);
    }

}
