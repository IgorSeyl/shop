package org.example.shop.database.service;

import org.example.shop.database.dao.RoleDAO;
import org.example.shop.model.Role;
import org.example.shop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleDAO roleDAO = new RoleDAO();

    public List<Role> findAllRoles() {
        return roleDAO.findAll();
    }

    public Role findRoleById(int id) {
        return roleDAO.findById(id);
    }

    public Role findRoleByName(String name) {
        return roleDAO.findByName(name);
    }

    public void saveRole(Role role) {
        roleDAO.save(role);
    }

    public void updateRole(Role role) {
        roleDAO.update(role);
    }

    public void deleteRole(Role role) {
        roleDAO.delete(role);
    }
}
