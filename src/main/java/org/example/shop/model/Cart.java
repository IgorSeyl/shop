package org.example.shop.model;

import org.example.shop.model.Book;
import org.example.shop.model.User;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "carts_igorseyl")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn (name="user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable (name="cart_book_igorseyl",
            joinColumns=@JoinColumn (name="cart_id"),
            inverseJoinColumns=@JoinColumn(name="book_id"))
    private List<Book> books;

    public Cart() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return id == cart.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
