package org.example.shop.model;

public enum Style {
    RUSSIAN_CLASSIC("Русская классика"),
    HORROR("Ужасы"),
    FANTASY("Фэнтэзи"),
    ROMANTIC("Романтика");

    private String value;

    Style(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
