<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Авторизация</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/authorization.css"/> "/>
</head>
<body>


<div class="form_auth_block">

    <div class="form_auth_block_content">

        <p class="form_auth_block_head_text">Авторизация</p>

        <form id="authorization_form" class="form_auth_style" action="${pageContext.request.contextPath}/" name="user"
              method="post">
            <input type="text" name="username" placeholder="Ваш логин" required>
            <input type="password" name="password" placeholder="Ваш пароль" required>
        </form>

        <div class="buttons">
            <button class="form_auth_button" type="submit" form="authorization_form">Войти</button>
            <form class="form_registration" action="${pageContext.request.contextPath}/registration">
                <button type="submit" class="registration">Регистрация</button>
            </form>
        </div>

        <c:if test="${incorrect}">
            <p>Неверный логин или пароль</p>
        </c:if>

    </div>
</div>

<%--<form action="/" name="user" method="post">--%>
<%--    <label>Логин</label>--%>
<%--    <input type="text" name="login" required>--%>
<%--    <label>Пароль</label>--%>
<%--    <input type="password" name="password" required>--%>
<%--    <button type="submit">Войти</button>--%>
<%--</form>--%>

</body>
</html>
