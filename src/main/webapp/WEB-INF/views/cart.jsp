<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Корзина</title>
</head>
<body>

<p>Корзина:</p>

<c:forEach var="cart" items="${cart}">
    <p>${cart.key.author} "${cart.key.bookName}" ${cart.key.price} руб. - ${cart.value} шт. (${cart.key.price * cart.value} руб.)</p>
</c:forEach>

<form:form method="POST" action="${pageContext.request.contextPath}/" modelAttribute="user">
    <input type="hidden" name="id" value=${user.id}>
<%--    <form:input type="hidden" value="${user.role}" path="role"/>--%>
    <button type="submit" class="exit">Вернуться в магазин</button>
</form:form>

</body>
</html>
