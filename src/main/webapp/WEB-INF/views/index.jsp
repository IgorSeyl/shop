<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html;charset=UTF-8" %>

<html>

<head>
    <title>Книжный магазин</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/index.css"/> "/>
</head>

<body>

<header class="header">
    <div class="header__inner">

        <h1>Книжный магазин</h1>

        <div class="cart-and-authorization">

            <form:form method="POST" action="/cart" modelAttribute="user">
<%--                <form:input id="role" type="hidden" value="${user.role}" path="role"/>--%>
                <input id="userId" type="hidden" name="id" value=${user.id}>
                <input class="cart" type="image" src=<c:url value="/resources/img/cart.png"/>>
            </form:form>

            <c:choose>
                <c:when test="${authorized}">

                    <p class="greetings">Привет, ${user.username}!</p>

                    <form action="${pageContext.request.contextPath}/account" name="user" method="post">

                        <input type="hidden" name="id" value=${user.id}>
                        <input class="account" type="image" src=<c:url value="/resources/img/account-pic.png"/>>
                    </form>

                    <form action="${pageContext.request.contextPath}/">
                        <input id="exit" class="exit" type="image" src=<c:url value="/resources/img/exit.png"/>>
                    </form>

                </c:when>
                <c:otherwise>

                    <form action="${pageContext.request.contextPath}/authorization">
                        <button type="submit" class="authorization">Авторизация</button>
                    </form>
                </c:otherwise>
            </c:choose>

        </div>

    </div>
</header>

<section class="products">

    <c:forEach var="book" items="${books}">

        <div class="product-card">

            <div class="product-info">
                <img class="product-image" src=${book.image}>
                <p>${book.author}</p>
                <p>${book.bookName}</p>
                <p>${book.style.value}</p>
                <p>${book.price} руб.</p>
                <button type="button" class="cd-add-to-cart" data-id=${book.id}>Купить</button>
            </div>
        </div>

    </c:forEach>

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/header.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cart.js"></script>

</body>

</html>
