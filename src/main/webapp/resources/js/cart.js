const buttons = document.getElementsByClassName("cd-add-to-cart");

//localStorage.clear();

function getUser(){
    return JSON.parse(localStorage.getItem("user"));
}

function setUser(id, role){
    let user = {id: id};
    return localStorage.setItem("user", JSON.stringify(user));
}

function getCartData(){
    return JSON.parse(localStorage.getItem('cart'));
}

function setCartData(o){
    localStorage.setItem('cart', JSON.stringify(o));
}


function postCart(){

    function mapToObj(inputMap) {
        let obj = {};

        inputMap.forEach(function(value, key){
            obj[key] = value
        });

        return obj;
    }

    let userCart = {
        userId: getUser().id,
        isAnon: getUser().isAnon,
        products: JSON.stringify(mapToObj(productsMap))
    };

    $.ajax({
        type: "POST",
        url: "/cart",
        contentType: "application/json",
        data: JSON.stringify(userCart),

        success: function () {
            console.log("Cart is up");
        },
        error : function() {
            console.log("AJAX error")}
    });
}
// -----------------------------------------------------------

setUser(
    document.getElementById("userId").getAttribute("value")
);

let productsMap = new Map(); // ключ - id товара, значение - количество

if (getCartData() != null) {

    for (let i = 0; i < getCartData().bookId.length; i++) {
        productsMap.set(getCartData().bookId[i], getCartData().bookQuantity[i]);
    }
}

postCart();

function addToCart(e) {

    let productId = e.getAttribute('data-id');

    if (productsMap.get(productId) >= 0) productsMap.set(productId, productsMap.get(productId) + 1);
    else productsMap.set(productId, 1);

    let productsToCart = {
        bookId: Array.from(productsMap.keys()),
        bookQuantity: Array.from(productsMap.values())
    };

    setCartData(productsToCart);

    postCart();
}

for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
        addToCart(buttons[i]);
    });
}

// cartForm.addEventListener("submit", function (event) {
//
//     event.preventDefault();
//
//     function mapToObj(inputMap) {
//         let obj = {};
//
//         inputMap.forEach(function(value, key){
//             obj[key] = value
//         });
//
//         return obj;
//     }
//
//     let userCart = {
//         userId: getUser().id,
//         isAnon: getUser().isAnon,
//         products: JSON.stringify(mapToObj(productsMap))
//     };
//
//     $.ajax({
//         type: "POST",
//         url: "/cart",
//         contentType: "application/json",
//         data: JSON.stringify(userCart),
//
//         success: function () {
//             console.log("success");
//             //window.location = '/cart';
//         },
//         error : function() {
//             console.log("AJAX error")}
//     });
// });